package ru.andreymarkelov.atlas.plugins;

public class RenderResult {
    private final String result;

    public RenderResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "RenderResult[result=" + result + "]";
    }
}
