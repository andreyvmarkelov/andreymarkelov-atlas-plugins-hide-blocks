package ru.andreymarkelov.atlas.plugins;

public class HiderMacroParams {
    private String blockName;
    private String label;
    private Boolean hideOnCheck;

    public HiderMacroParams() {}

    public HiderMacroParams(String blockName, String label, Boolean hideOnCheck) {
        this.blockName = blockName;
        this.label = label;
        this.hideOnCheck = hideOnCheck;
    }

    public String getBlockName() {
        return blockName;
    }

    public Boolean getHideOnCheck() {
        return hideOnCheck;
    }

    public String getLabel() {
        return label;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public void setHideOnCheck(Boolean hideOnCheck) {
        this.hideOnCheck = hideOnCheck;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "HiderMacroParams[blockName=" + blockName + ", label=" + label + ", hideOnCheck=" + hideOnCheck + "]";
    }
}
