package ru.andreymarkelov.atlas.plugins;

public class PanelMacroParams {
    private String blockName;
    private String className;

    public PanelMacroParams() {}

    public PanelMacroParams(String blockName, String className) {
        this.blockName = blockName;
        this.className = className;
    }

    public String getBlockName() {
        return blockName;
    }

    public String getClassName() {
        return className;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public String toString() {
        return "PanelMacroParams[blockName=" + blockName + ", className=" + className + "]";
    }
}
