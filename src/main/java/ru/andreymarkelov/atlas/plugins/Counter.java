package ru.andreymarkelov.atlas.plugins;

public class Counter {
    private static long curr = System.currentTimeMillis();

    private Counter() {}

    public static synchronized long getValue() {
        return curr++;
    }
}
